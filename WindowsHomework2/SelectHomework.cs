﻿namespace Homework2_2
{
    class Program
    {
        class Person
        {
            protected string name;
            protected uint age;

            public string Name
            {
                get { return name; }
                set { name = value; }
            }

            public uint Age
            {
                get { return age; }
                set { age = value; }
            }

            public Person(string name, uint age)
            {
                this.name = name;
                this.age = age;
            }
        }

        public interface ISelectHomework
        {
            void SelectHomework();
        }

        class Teacher : Person, ISelectHomework
        {
            public Teacher(string name, uint age) : base(name, age) { }

            public void SelectHomework()
            {
                System.Console.WriteLine("{0}收作业!", name);
            }
        }

        class Student : Person, ISelectHomework
        {
            public Student(string name, uint age) : base(name, age) { }

            public void SelectHomework()
            {
                System.Console.WriteLine("{0}交作业！", name);
            }
        }

/*        public static void Main(string[] args)
        {
            Student s = new Student("学生D", 20);
            Teacher t = new Teacher("老师A", 40);

            ((ISelectHomework)s).SelectHomework();
            ((ISelectHomework)t).SelectHomework();
        }*/
    }
}