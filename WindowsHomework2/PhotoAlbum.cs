﻿namespace Homework2_1
{
    class Photo
    {
        private string name;
        private string url;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string Url
        {
            get { return url; }
            set { url = value; }
        }

        public Photo(string name, string url)
        {
            this.name = name;
            this.url = url;
        }
    }

    class Album
    {
        private string name;
        private Photo[] data = new Photo[5];

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public Photo this[uint index]
        {
            get { return data[index]; }
            set { data[index] = value; }
        }

        public void ShowPhotos()
        {
            foreach (Photo p in data)
            {
                if (p == null) break;
                System.Console.WriteLine(p.Name);
                System.Console.WriteLine(p.Url);
                System.Console.WriteLine();
            }
        }

        public Album(string name)
        {
            this.name=name;
        }

/*        public static void Main(string[] args)
        {
            Album myalbum = new Album("DobyAsa's Album");
            Photo photo1 = new Photo("ClassRoom", "www.dobyasa.com/photo1");
            Photo photo2 = new Photo("BedRoom", "www.dobyasa.com/photo2");

            myalbum[0] = photo1;
            myalbum[1] = photo2;

            myalbum.ShowPhotos();

            myalbum[1] = photo1;
            myalbum.ShowPhotos();

            photo1.Name = "LivingRoom";
            myalbum.ShowPhotos();
        }*/

    }


}